﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Ecommerce.WebApi.Controllers
{
    public class UsersController : ApiController
    {

        public IEnumerable<User> GetAllUsers()
        {
            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                return ctx.Users.ToArray();
            }
        }

        public IHttpActionResult GetUser(int id)
        {

            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                var user = ctx.Users.FirstOrDefault((u) => u.UserId == id);
                if (user == null)
                {
                    return NotFound();
                }
                return Ok(user);
            }

        }

    }
}