(function () {
    'use strict';
    var app = angular.module('ecommerceApp', ['ngRoute']);

    app.config(function ($routeProvider, $locationProvider) {

        $routeProvider

        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })

        .when('/products', {
            templateUrl: 'pages/products.html',
            controller: 'productsController'
        })

        .when('/orders', {
            templateUrl: 'pages/orders.html',
            controller: 'ordersController' 
        })

        .otherwise({
            redirectTo: '/'
        })

        $locationProvider.html5Mode(true);
    });
})();